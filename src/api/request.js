import axios from 'axios';
import { getCurrentToken, evictPersistedToken } from './auth';

const apiBaseUrl = process.env.REACT_APP_API_BASE_URL;

const instance = axios.create({
    baseURL: apiBaseUrl
});

let globalErrorListener;

// Cache to reduce memory footprint
const neverResolvingPromise = new Promise(() => { });

// Add the current API token into the `Authorization` header

instance.interceptors.request.use(config => {
    const token = getCurrentToken();

    if (token !== null) {
        // Add token header
        config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
});

function canComponentHandleError(error) {
    return error.response && error.response.status !== 500;
}

function checkAuthorized(error) {
    if (error.response && error.response.status === 401) {
        // Force logout and render UnauthenticatedApp
        evictPersistedToken();
        return false;
    }

    return true;
}

// Catch all request errors and, depending on the type of error,
// pass the exception to the next handler or throw a global error.

instance.interceptors.response.use(
    response => {
        if (response.data && response.data.success) {
            // Resolve with API JSON data
            return Promise.resolve(response.data);
        }

        console.warn('Got a success HTTP code on a non-successful API call');
        console.warn(response);

        // Let the component handle the API error
        return Promise.reject(response.data || { message: 'Unknown error' });
    },
    error => {
        if (!checkAuthorized(error)) {
            return neverResolvingPromise;
        }

        if (canComponentHandleError(error)) {
            // The network request was succesful, but the API returned a non-fatal error.
            // Let the component handle the API error.
            return Promise.reject(error.response.data.title || error.response.data.message);
        }

        // Intercept the network error and throw a global error.
        throwGlobalError(error);

        // The component that made the network request will
        // be unmounted when the error page is rendered.
        return neverResolvingPromise;
    }
);

function throwGlobalError(error) {
    if (typeof error === 'string') {
        // Wrap message in `Error`
        error = new Error(error);
    }

    // Notify global error listener
    if (globalErrorListener) {
        globalErrorListener(error);
    } else {
        // The page hasn't fully loaded, but maybe Sentry is initialized?
        throw error;
    }
}

/**
 * Updates the error listener called when a request fails
 * because a network error or a fatal API error occurred.
 *
 * @param {Function} listener the error listener
 */
export function setGlobalErrorListener(listener) {
    if (!listener) {
        throw new Error(
            `Expected non-null global error listener, got ${listener} instead`
        );
    }

    globalErrorListener = listener;
}

export default instance;

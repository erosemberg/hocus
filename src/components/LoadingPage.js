import React from 'react';

import SingleContainer from './design/Container/SingleContainer';
import Container from './design/Container';

function LoadingPage() {
    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className='columns'>
                    <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto text-center'>
                        <h1>✨ 🔮 ✨</h1>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default React.memo(LoadingPage);
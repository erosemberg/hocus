import React, { useRef, useState, useEffect } from 'react';
import axios from '../../api/request';
import useForm from '../../hooks/form';
import classNames from 'classnames';
import { useAuth } from '../../api/auth';
import { useHistory } from 'react-router-dom';

import SingleContainer from '../design/Container/SingleContainer';
import Container from '../design/Container';
import WinCard from '../design/Card/WinCard';
import Toast from '../design/Toast';

import { default as HocusLogo } from '../../assets/hocus.png';
import { default as Horris } from '../../assets/horris.jpg';
import Input from '../design/Input';
import Button from '../design/Button';
import FormGroup from '../design/Input/FormGroup';

function CreateAccountPage({ inviteCode }) {
    const fullName = useRef();
    const email = useRef();
    const username = useRef();
    const password = useRef();

    const history = useHistory();
    const { login } = useAuth();

    const [available, setAvailable] = useState(true);
    const [handle, setHandle] = useState('');

    const handleSubmit = () => {
        if (!available) {
            return Promise.reject();
        }
        return axios.post('/account/create', {
            email: email.current.value,
            username: username.current.value,
            name: fullName.current.value,
            password: password.current.value,
            inviteCode: inviteCode
        }).then(({ token }) => {
            login(token, true);
            history.push('/flow/setup_account');
        })
    }

    useEffect(() => {
        const checkNameValidity = () => {
            if (handle) {
                // this needs to check all handle availabilities, not just account usernames.
                axios.get(`/account/checkUsername?username=${handle}`).then(({ available }) => {
                    setAvailable(available);
                });
            } else {
                setAvailable(true);
            }
        }

        let timeoutFunction = setTimeout(() => {
            checkNameValidity();
        }, 500);
        return () => clearTimeout(timeoutFunction);
    }, [handle]);

    function onHandleChange(event) {
        event.preventDefault();
        setHandle(event.target.value);
    }

    const { isSubmitting, onSubmit, error } = useForm(handleSubmit, { clearOnSucess: false });

    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className='columns'>
                    <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <WinCard orgLogo={HocusLogo} avatar={Horris} userName='Horris H.' orgName='Hocus' timeAgo='1m'>
                            { /* eslint-disable-next-line jsx-a11y/anchor-is-valid  */}
                            <p className='text-large mb-4'><span className='text-bold'>Welcome to Hocus!</span> Just need a bit of info from you and then I can grant you access to the magical world I'm building.</p>
                            {error && <Toast className='toast-error'>{error}</Toast>}
                            <form onSubmit={onSubmit}>
                                <Input
                                    required
                                    label='Full Name'
                                    type='text'
                                    placeholder='Horris H.'
                                    className='input-lg'
                                    ref={fullName}
                                />
                                <Input
                                    required
                                    label='Email'
                                    type='email'
                                    placeholder='horris@joinhocus.com'
                                    className='input-lg'
                                    ref={email}
                                />
                                <FormGroup className={classNames(available ? '' : 'has-error')}>
                                    <Input
                                        required
                                        label='Username'
                                        type='text'
                                        placeholder='@horris'
                                        className='input-lg'
                                        pattern='^(\w){1,15}$'
                                        maxLength={15}
                                        value={handle}
                                        onChange={onHandleChange}
                                        ref={username}
                                    />
                                    {!available && <p className='form-input-hint'>That handle is already taken!</p>}
                                </FormGroup>
                                <Input
                                    required
                                    label='Password'
                                    type='password'
                                    placeholder='**********'
                                    className='input-lg'
                                    minLength={8}
                                    ref={password}
                                />
                                <Button type='submit' className={classNames('btn-lg btn-block btn-yellow mt-4', isSubmitting ? 'disabled' : '')}>{isSubmitting ? 'Working...' : 'Sign Up'}</Button>
                            </form>
                        </WinCard>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default CreateAccountPage;

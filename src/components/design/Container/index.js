import React from 'react'
import classNames from 'classnames';

function Container({ className, children }) {
    const containerClass = classNames('container', className);
    return (
        <div className={containerClass}>
            {children}
        </div>
    )
}

export default React.memo(Container);
import React from 'react';
import classNames from 'classnames';

function Column({ className, children }) {
    const columnClass = classNames('column', className);
    return (
        <div className={columnClass}>
            {children}
        </div>
    )
}

export default React.memo(Column);
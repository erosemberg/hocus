import React, { useRef } from 'react';
import { useHistory, Link } from 'react-router-dom';
import classNames from 'classnames';

import axios from '../api/request';
import { useAuth } from '../api/auth';
import useForm from '../hooks/form';

import SimpleCard from './design/Card/SimpleCard';
import Container from './design/Container';
import SingleContainer from './design/Container/SingleContainer';
import Button from './design/Button';
import Input from './design/Input';
import Toast from './design/Toast';

import { default as hocusLogo } from '../assets/hocus.png';
import { default as horris } from '../assets/horris.jpg';

function SignInPage() {
    const history = useHistory();
    const { login } = useAuth();

    const userEmail = useRef();
    const password = useRef();

    function handleSubmit() {
        const userValue = userEmail.current.value;
        const passwordValue = password.current.value;

        return axios
            .post('/account/login', {
                login: userValue,
                password: passwordValue
            })
            .then(({ token }) => {
                login(token, true);
                history.replace('/');
            });
    }

    const { isSubmitting, onSubmit, error } = useForm(handleSubmit);

    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className='columns'>
                    <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <SimpleCard userName='Horris H.' orgName='Hocus' timeAgo='1m' orgLogo={hocusLogo} avatar={horris}>
                            <p className="text-large mb-4">Welcome back to Hocus!</p>
                            {error && <Toast className='toast-error'>{error}</Toast>}
                            <form onSubmit={onSubmit}>
                                <Input
                                    label='Email / Username'
                                    type='text'
                                    placeholder='horris@joinhocus.com'
                                    className='input-lg'
                                    ref={userEmail}
                                />
                                <Input
                                    label='Password'
                                    type='password'
                                    placeholder='**********'
                                    className='input-lg'
                                    ref={password}
                                />
                                <Button size='btn-lg' className={classNames('btn-block mt-4', isSubmitting ? 'disabled' : '')} type='submit'>
                                    Sign In
                                </Button>
                            </form>
                        </SimpleCard>
                        <div className='text-center mt-4'>
                            <p className='text-small'>Don't have access yet? <Link to='/waitlist'>Join the waitlist.</Link></p>
                        </div>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default SignInPage;
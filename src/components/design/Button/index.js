import React from 'react';
import classNames from 'classnames';

function Button({ color = 'blue', size = '', className, children, ...props }) {
    const buttonClass = classNames('btn', size, color, className);
    return (
        <button {...props} className={buttonClass}>
            {children}
        </button>
    )
}

export default React.memo(Button);
import React from 'react';
import classNames from 'classnames';

function ButtonGroup({ className, children }) {
    const bgClass = classNames('btn-group', className);
    return (
        <div className={bgClass}>
            {children}
        </div>
    )
}

export default React.memo(ButtonGroup);
import React, { useContext, useState } from 'react';

// localStorage item key
const LOCAL_TOKEN_KEY = 'token';

/**
 * Retrieves the API token from disk.
 *
 * @returns {String} the API token, or `null` if not present
 */
function getTokenFromDisk() {
    return localStorage.getItem(LOCAL_TOKEN_KEY);
}

/**
 * Persists the given API token to disk.
 */
function persistToken(value) {
    localStorage.setItem(LOCAL_TOKEN_KEY, value);
}

/**
 * Clears the persisted API token from disk.
 */
function evictPersistedToken() {
    localStorage.removeItem(LOCAL_TOKEN_KEY);
}

/**
 * The current loaded API token.
 *
 * Accesors must be aware this field should only be used
 * by utilities that are not necessarily called from a
 * React component.
 *
 * If possible, always use the `useAuth` hook to get the token.
 */
let currentToken = getTokenFromDisk();

/**
 * Gets the current API token.
 *
 * Users must be aware this method should only be called by
 * utilities that do not have access to the `useAuth` hook.
 *
 * @see currentToken for more details
 */
function getCurrentToken() {
    return currentToken;
}

// Auth provider and hook

const AuthContext = React.createContext();

function AuthProvider(props) {
    const [token, setToken] = useState(currentToken);

    const login = (newToken, remember) => {
        if (token !== null) {
            throw new Error('User is already authenticated');
        }

        // Set current token
        setToken(newToken);
        currentToken = newToken;

        if (remember) {
            persistToken(newToken);
        }
    };

    const logout = () => {
        if (token === null) {
            throw new Error('User is not authenticated');
        }

        // Clear current token
        setToken(null);
        currentToken = null;

        evictPersistedToken();
    };

    return (
        <AuthContext.Provider
            {...props}
            value={{ token, login, logout }}
        ></AuthContext.Provider>
    );
}

function useAuth() {
    const context = useContext(AuthContext);

    if (context === undefined) {
        throw new Error('useAuth must be used within an AuthProvider');
    }

    return context;
}

export { getCurrentToken, AuthProvider, useAuth, evictPersistedToken };

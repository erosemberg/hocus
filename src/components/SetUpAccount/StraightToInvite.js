import React from 'react';
import { useMultiStepState } from '../MultiStep';

import Button from '../design/Button';

function StraightToInvite() {
    const { next } = useMultiStepState();

    return (
        <div className='text-right'>
            <Button onClick={next} className='btn-lg btn-yellow'>Next</Button>
        </div>
    )
}

export default React.memo(StraightToInvite);

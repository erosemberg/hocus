import React, { useContext, useState, useEffect } from 'react';

const MultiStepContext = React.createContext();
const StepContext = React.createContext();

function MultiStepProvider({ children }) {
    const childSteps = React.Children.toArray(children);
    const size = childSteps.length;
    const [current, setCurrent] = useState(0);
    const [progress, setProgress] = useState(0);

    useEffect(() => {
        if (current === 0) {
            setProgress(0);
        } else if (current === (size - 1)) {
            setProgress(1);
        } else {
            setProgress(current / (size - 1));
        }
    }, [current, setProgress, size]);

    const next = () => {
        if (current < (size - 1)) {
            setCurrent(current + 1);
        } else {
            throw new Error(`can't move forward, no value`)
        }
    };

    const prev = () => {
        if (current > 0) {
            setCurrent(current - 1);
        } else {
            throw new Error(`can't move back on this Step, already first`);
        }
    }

    const context = {
        current,
        next,
        prev,
        progress
    };

    const renderable = React.Children.map(children, (child, index) => {
        const stepContext = {
            index,
            isLast: index === (size - 1),
            isFirst: index === 0
        };
        return (
            <StepContext.Provider value={stepContext}>
                {child}
            </StepContext.Provider>
        )
    });

    return (
        <MultiStepContext.Provider value={context}>
            {renderable}
        </MultiStepContext.Provider>
    )
}

function Step({ component }) {
    const { index } = useStepState();
    const { current } = useMultiStepState();

    if (index === current) {
        return component;
    }

    return null;
}

function useMultiStepState() {
    const context = useContext(MultiStepContext);
    if (context === undefined) {
        throw new Error("useMultiStepState must be referenced within a MultiStepProvider");
    }

    return context;
}

function useStepState() {
    const context = useContext(StepContext);
    if (context === undefined) {
        throw new Error("useStepState can only be called within a MultiStepProvider");
    }

    return context;
}

export { MultiStepProvider, Step, useMultiStepState, useStepState };
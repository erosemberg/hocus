import { useState, useCallback } from 'react';

/**
 * A form submission handler hook. Supports error messages.
 *
 * The identities of `setError` and `clearSubmit` are stable and
 * won't change between renders.
 *
 * @param {Function} submitHandler the `onSubmit` callback (can return a `Promise`)
 * @param {Object} options hook options
 * @param {boolean} options.handleErrors whether the hook should handle exceptions thrown
 *        by the `submitHandler` returned `Promise`
 * @param {boolean} options.clearOnSucess whether to clear the "submitting" state on success
 */
function useForm(
    submitHandler,
    { handleErrors = true, clearOnSuccess = true } = {}
) {
    const [isSubmitting, setSubmitting] = useState(false);
    const [error, setErrorMessage] = useState();

    const clearSubmit = useCallback(() => setSubmitting(false), []);
    const setError = useCallback(
        error => {
            if (error instanceof Error) {
                error = error.message;
            }

            setErrorMessage(error);
            clearSubmit();
        },
        [clearSubmit]
    );

    function onSubmit(event) {
        if (event) {
            // Cancel default form submission
            event.preventDefault();
        }

        if (isSubmitting) {
            // Prevent multiple ongoing submissions
            return;
        }

        setSubmitting(true);

        try {
            let promise = submitHandler();

            if (promise) {
                if (clearOnSuccess) {
                    promise = promise.then(clearSubmit);
                }

                if (handleErrors) {
                    // Catch API errors
                    promise = promise.catch(setError);
                }
            } else if (clearOnSuccess) {
                clearSubmit();
            }
        } catch (err) {
            console.log('caught here', err);
            if (handleErrors) {
                setError(err);
            } else {
                // Rethrown unhandled exception
                throw err;
            }
        }
    }

    return { isSubmitting, onSubmit, error, setError, clearSubmit };
}

export default useForm;

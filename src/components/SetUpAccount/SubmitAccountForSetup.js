import React, { useEffect, useState } from 'react';
import axios from '../../api/request';
import { useSetUpState } from './setup';

import LoadingPage from '../LoadingPage';
import ErrorPage from '../ErrorPage';

function SubmitAccountForSetup() {
    const data = useSetUpState();
    const [error, setError] = useState(null);

    useEffect(() => {
        const { organization, ...other } = data;

        async function doAccountSetup() {
            return axios.post('/account/setup', other).catch(setError);
        }

        async function createOrganization() {
            const { logo, blob, ...rest } = organization;
            return axios.post('/organization/create', rest).catch(setError);
        }

        async function uploadFile(organizationId) {
            const { blob } = organization;
            let formData = new FormData();
            formData.append('file', blob);
            return axios.put(`/organization/settings/logo?orgId=${organizationId}`, formData, {
                headers: {
                    'accept': 'application/json',
                    'Content-Type': `multipart/form-data; boundary=${formData.boundary}`
                }
            }).catch(setError);
        }

        async function executeCreate() {
            await doAccountSetup();
            if (other.selected === 'founder') {
                const { organizationId } = await createOrganization();
                await uploadFile(organizationId);
            }
        }

        executeCreate();
    }, [data]);

    if (error) {
        return (
            <ErrorPage>{error}</ErrorPage>
        )
    }

    return (
        <LoadingPage />
    )
}

export default SubmitAccountForSetup;
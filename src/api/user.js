import React, { useContext, useEffect, useState } from 'react';
import axios from './request';

import LoadingPage from '../components/LoadingPage';

const UserContext = React.createContext();

function UserProvider({ token, ...props }) {
    const [user, setUser] = useState(null);

    useEffect(() => {
        // Fetch user data from API
        axios.get('/account').then(({ account: accountData }) => {
            setUser(accountData);
        });
    }, [token]);

    const isLoading = user === null;

    if (isLoading) {
        return <LoadingPage />;
    }

    return <UserContext.Provider {...props} value={user} />;
}

function useUser() {
    const context = useContext(UserContext);

    if (context === undefined) {
        throw new Error('useUser must be used within a UserProvider');
    }

    return context;
}

function getAvatar(name, size = 32) {
    return `https://ui-avatars.com/api/?background=ffe8d9&color=f35627&size=${size}&name=${name}`;
}

export { UserProvider, useUser, getAvatar };

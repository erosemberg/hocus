import React from 'react';
import { Link } from 'react-router-dom';

import SingleContainer from './design/Container/SingleContainer';
import Container from './design/Container';

import { default as LostHorris } from '../assets/lost-horris.png';

function NotFoundPage() {
    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className='columns'>
                    <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <div className='text-center'>
                            <img src={LostHorris} alt='Lost Horris!' />
                            <h3>Oops, you seem to have gotten lost</h3>
                            <Link to='/' className='btn btn-lg'>Take me home</Link>
                        </div>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default React.memo(NotFoundPage);

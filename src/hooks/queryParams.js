import { useLocation } from 'react-router-dom';

export default function useQueryParams() {
    const { search: raw } = useLocation();
    const urlParams = new URLSearchParams(raw.substring(1));
    return Object.fromEntries(urlParams);
}
import React, { useState, useEffect } from 'react';
import axios from '../api/request';
import { useParams } from 'react-router-dom';
import classNames from 'classnames';

import Container from './design/Container';
import SingleContainer from './design/Container/SingleContainer';
import WinCard from './design/Card/WinCard';

import { default as HocusLogo } from '../assets/hocus.png';
import { default as Horris } from '../assets/horris.jpg';
import LoadingPage from './LoadingPage';
import Button from './design/Button';
import TwitterIcon from './icons/TwitterIcon';
import Toast from './design/Toast';

function InviteConfirm() {
    const [handle, setHandle] = useState(null);
    const [isEmail, setIsEmail] = useState(null);
    const [waiting, setWaiting] = useState(false);
    const [claimed, setClaimed] = useState(false);
    const [emailSent, setEmailSent] = useState(false);
    const { inviteId } = useParams();

    useEffect(() => {
        axios.get(`/invite/validate/${inviteId}`).then(({ claimed, twitterId, handle, isEmail }) => {
            if (claimed) {
                setClaimed(true);
            }

            if (twitterId && handle) {
                setHandle(handle);
                localStorage.setItem('twitterId', twitterId);
            }

            if (isEmail) {
                setIsEmail(true);
            }
        });
    }, [setHandle, inviteId, setIsEmail, setClaimed]);

    function verifyTwitter() {
        setWaiting(true);

        axios.post('/register/twitter/token').then(({ token }) => {
            const url = `https://api.twitter.com/oauth/authenticate?oauth_token=${token}`;
            window.location = url;
        });
    }

    function verifyEmail() {
        setWaiting(true);

        axios.post('/waitlist/email/verify', {
            inviteId: inviteId
        }).then(({ sent, claimed }) => {
            if (claimed) {
                setClaimed(true);
            }
            if (sent) {
                setEmailSent(true);
            }
            setWaiting(false);
        })
    }

    if (!handle && !isEmail) {
        return (
            <LoadingPage />
        )
    }

    // todo handle claimed
    if (claimed) {

    }

    if (handle) { // it's a twitter invite, we need to verify with twitter
        return (
            <SingleContainer className='d-flex flex-v-center flex-c-center'>
                <Container className='grid-lg'>
                    <div className='columns'>
                        <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                            <WinCard orgLogo={HocusLogo} avatar={Horris} orgName='Hocus' userName='Horris H.' timeAgo='5m'>
                                { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                <p className='text-large mb-4'><span className='text-bold'>You've been invited to Hocus <a className='user-tag'>@{handle}</a>!</span> Please verify with Twitter to register an account!</p>
                                <div className='text-center'>
                                    <Button onClick={verifyTwitter} className={classNames('btn-twitter', waiting ? 'disabled' : '')}><TwitterIcon /> {waiting ? 'Working...' : 'Verify with Twitter'}</Button>
                                </div>
                            </WinCard>
                        </div>
                    </div>
                </Container>
            </SingleContainer>
        )
    } else if (isEmail) {
        return (
            <SingleContainer className='d-flex flex-v-center flex-c-center'>
                <Container className='grid-lg'>
                    <div className='columns'>
                        <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                            <WinCard orgLogo={HocusLogo} avatar={Horris} orgName='Hocus' userName='Horris H.' timeAgo='5m'>
                                {emailSent && <Toast>We've sent you a confirmation link, check your inbox!</Toast>}
                                { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                <p className='text-large mb-4'><span className='text-bold'>You've been invited to Hocus!</span> Please verify your e-mail below to register an account!</p>
                                <div className='text-center'>
                                    <Button onClick={verifyEmail} className={classNames(waiting ? 'disabled' : '')}>{waiting ? 'Working...' : 'Send Verification'}</Button>
                                </div>
                            </WinCard>
                        </div>
                    </div>
                </Container>
            </SingleContainer>
        )
    } else {
        return (
            <SingleContainer className='d-flex flex-v-center flex-c-center'>
                <Container className='grid-lg'>
                    <div className='columns'>
                        <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                            <WinCard orgLogo={HocusLogo} avatar={Horris} orgName='Hocus' userName='Horris H.' timeAgo='5m'>
                                { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                <p className='text-large mb-4'><span className='text-bold'>Huh, interesting.</span> We couldn't figure out what kind of invite that was, please try again.</p>
                            </WinCard>
                        </div>
                    </div>
                </Container>
            </SingleContainer>
        )
    }
}

export default InviteConfirm;

import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

import SetUpAccount from './SetUpAccount';
import NotFoundPage from './NotFoundPage';
import FeedPage from './FeedPage';

function AuthenticatedApp() {
    return (
        <Router>
            <Switch>
                <Route path='/' exact>
                    <FeedPage />
                </Route>
                <Route path='/flow/setup_account' exact>
                    <SetUpAccount />
                </Route>
                <Route>
                    <NotFoundPage />
                </Route>
            </Switch>
        </Router>
    )
}

export default React.memo(AuthenticatedApp);
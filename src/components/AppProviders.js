import React from 'react';

import { AuthProvider } from '../api/auth';
import ErrorBoundary from './ErrorBoundary';

/**
 * Provides the global context of Hocus.
 */
function AppProviders({ children }) {

    function doAlert() {
        console.group('Alert');
        console.log('%cHold your Broomsticks!', 'font-size: 40px; font-weight: bold; line-height: normal; color: #2ED0A2;');
        console.log('%cPlease be careful when pasting or writing things here as they may be exposing your precious data!', 'font-size: 15px; line-height: normal');
        console.groupEnd();
    }

    doAlert();

    return (
        <ErrorBoundary>
            <AuthProvider>{children}</AuthProvider>
        </ErrorBoundary>
    );
}

export default AppProviders;

import React from 'react';

import { useAuth } from '../api/auth';
import { UserProvider } from '../api/user';

import LoadingPage from './LoadingPage';

const UnauthenticatedApp = React.lazy(() => import('./UnauthenticatedApp'));
const AuthenticatedApp = React.lazy(() => import('./AuthenticatedApp'));

function App() {
    const { token } = useAuth();

    return (
        <React.Suspense fallback={<LoadingPage />}>
            {token ? (
                <UserProvider token={token}>
                    <AuthenticatedApp />
                </UserProvider>
            ) : (
                    <UnauthenticatedApp />
                )}
        </React.Suspense>
    );
}

export default App;

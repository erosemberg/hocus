import React from 'react'
import classNames from 'classnames';

function SingleContainer({ className, children }) {
    const containerClass = classNames('single-container', className);
    return (
        <div className={containerClass}>
            {children}
        </div>
    )
}

export default React.memo(SingleContainer);
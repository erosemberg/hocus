import React from 'react';
import classNames from 'classnames';

const Input = React.forwardRef((
    { label, className, labelClassName, ...props },
    ref
) => {
    const inputClass = classNames('form-input', className);
    const labelClass = classNames('form-label', labelClassName);

    return (
        <>
            <label className={labelClass}>{label}</label>
            <input {...props} ref={ref} className={inputClass} />
        </>
    )
})

export default React.memo(Input);
import React from 'react'
import classNames from 'classnames';

function Card({ className, children }) {
    const cardclass = classNames('card', className);

    return <div className={cardclass}>
        {children}
    </div>
}

export default React.memo(Card);
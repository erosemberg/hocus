import React from 'react';

function CardHeader({ children }) {
    return (
        <div className='card-header'>
            {children}
        </div>
    )
}

export default React.memo(CardHeader);
import React from 'react';
import classNames from 'classnames';

function FormGroup({ className, children }) {
    const divClass = classNames('form-group', className);
    return (
        <div className={divClass}>
            {children}
        </div>
    )
}

export default React.memo(FormGroup);
import PropTypes from 'prop-types';

export const componentType = PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func
]);

export const renderableType = PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
]);

export default PropTypes;
import React from 'react';
import classNames from 'classnames';

function LinkButton({ href, color = 'blue', size = '', className, children }) {
    const buttonClass = classNames('btn', size, color, className);
    return (
        <a href={href} className={buttonClass}>
            {children}
        </a>
    )
}

export default React.memo(LinkButton);
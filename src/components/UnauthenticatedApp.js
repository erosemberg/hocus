import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

import SignInPage from './SignInPage';
import WaitList from './WaitListPage';
import WaitListJoinPage from './WaitListJoinPage';
import SignUp from './SignUp';
import InviteConfirm from './InviteConfirm';
import TwitterVerifiedFlow from './TwitterVerifiedFlow';
import NotFoundPage from './NotFoundPage';
import EmailVerifiedFlow from './EmailVerifiedFlow';

function UnauthenticatedApp() {
    return (
        <Router>
            <Switch>
                <Route path='/' exact>
                    <WaitList />
                </Route>
                <Route path='/join' exact>
                    <WaitListJoinPage />
                </Route>
                <Route path='/sign_in' exact>
                    <SignInPage />
                </Route>
                <Route path='/flow/sign_up' exact>
                    <SignUp />
                </Route>
                <Route path='/join/:inviteId'>
                    <InviteConfirm />
                </Route>
                <Route path='/flow/twitter' exact>
                    <TwitterVerifiedFlow />
                </Route>
                <Route path='/flow/email/:verification' exact>
                    <EmailVerifiedFlow />
                </Route>
                <Route>
                    <NotFoundPage />
                </Route>
            </Switch>
        </Router>
    )
}

export default React.memo(UnauthenticatedApp);
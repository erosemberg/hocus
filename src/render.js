import React from 'react';
import ReactDOM from 'react-dom';

import AppProviders from './components/AppProviders';
import App from './components/App';

export default function render() {
    ReactDOM.render(
        <AppProviders>
            <App />
        </AppProviders>,
        document.getElementById('root')
    );
}

import React from 'react';
import { setGlobalErrorListener as setApiErrorListener } from '../api/request'
import ErrorPage from './ErrorPage';

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { error: null };
        setApiErrorListener(this.setError);
    }

    static getDerivedStateFromError(error) {
        return { error };
    }

    componentDidCatch(error, errorInfo) {
        // todo sentry
    }

    setError = raw => {
        const error = raw.reason || raw;

        this.setState({ error });
    }

    componentDidMount() {
        window.addEventListener('unhandledrejection', this.setError);
    }

    componentWillUnmount() {
        window.removeEventListener('unhandledrejection', this.setError);
    }

    render() {
        const { error } = this.state;

        if (error) {
            return (
                <ErrorPage>{error && error.toString()}</ErrorPage>
            )
        }

        return this.props.children;
    }
}

export default ErrorBoundary;
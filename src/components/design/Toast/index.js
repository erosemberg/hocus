import React from 'react'
import classNames from 'classnames';

function Toast({ className, children }) {
    const toastClass = classNames('toast', className);
    return (
        <div className={toastClass}>
            {children}
        </div>
    )
}

export default React.memo(Toast);

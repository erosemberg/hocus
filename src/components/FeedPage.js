import React from 'react';
import { useUser } from '../api/user';

function FeedPage() {
    const { name } = useUser();

    return (
        <div>
            <h1>hey welcome to your feed, {name}</h1>
        </div>
    )
}

export default FeedPage;
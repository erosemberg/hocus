function readFileBlobAsync(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();

        reader.onload = () => {
            resolve(reader.result);
        }

        reader.onerror = reject;
        reader.readAsArrayBuffer(file);
    });
}

function readFileURLAsync(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();

        reader.onload = () => {
            resolve(reader.result);
        }

        reader.onerror = reject;
        reader.readAsDataURL(file);
    });
}

function readImageDataAsync(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();

        reader.onload = (e) => {
            let img = new Image();
            img.src = e.target.result;

            img.onload = function () {
                const data = {
                    height: this.height,
                    width: this.width,
                    size: Math.round((file.size / 1024))
                }

                resolve(data);
            }
        }

        reader.onerror = reject;
        reader.readAsDataURL(file);
    });
}

export { readFileBlobAsync, readFileURLAsync, readImageDataAsync }
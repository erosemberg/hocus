import React from 'react';
import SingleContainer from './design/Container/SingleContainer';
import Container from './design/Container';
import AskCard from './design/Card/AskCard';
import { default as hocusLogo } from '../assets/hocus.png';
import { default as horris } from '../assets/horris.jpg';

function ErrorPage({ children }) {
    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className='columns'>
                    <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <AskCard userName='Horris H.' orgName='Hocus' timeAgo='1m' orgLogo={hocusLogo} avatar={horris}>
                            <p className="text-large mb-4">An error ocurred and our team was notified. In the meantime, try refreshing.</p>
                            <div className='text-center'>
                                <code>{children}</code>
                            </div>
                        </AskCard>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default React.memo(ErrorPage);

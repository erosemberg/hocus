import React, { useRef, useState, useEffect } from 'react';
import { useMultiStepState } from '../MultiStep';
import { useSetUpActions, useSetUpState } from './setup';
import { readFileURLAsync, readImageDataAsync } from '../../util/files';
import axios from '../../api/request';
import classNames from 'classnames';

import Toast from '../design/Toast';
import Button from '../design/Button';
import Input from '../design/Input';
import Columns from '../design/Columns';
import Column from '../design/Columns/Column';
import FormGroup from '../design/Input/FormGroup';

const validImages = [
    'image/jpeg',
    'image/png'
];

function FounderOnboarding() {
    const { next } = useMultiStepState();
    const { edit } = useSetUpActions();
    const { organization } = useSetUpState();
    const { logo } = organization || {};
    const [imageError, setImageError] = useState(null);
    const [available, setAvailable] = useState(true);

    const companyName = useRef();
    const companyLogo = useRef();
    const companyHandle = useRef();

    const { name } = organization || {};
    const { handle } = organization || {};

    async function handleLogoChange(event) {
        event.stopPropagation();
        event.preventDefault();

        setImageError(null);
        const relativeFile = event.target.files[0];
        if (relativeFile) {
            if (!validImages.includes(relativeFile.type)) {
                setImageError('Image must be a JPEG or a PNG');
                return;
            }

            try {
                const fileData = await readImageDataAsync(relativeFile);
                if ((fileData.height > 1024 || fileData.width > 1024)) {
                    setImageError('Image must be no greater than 1024x1024');
                    return;
                }
                if (fileData.size > 2048) {
                    setImageError('Image must be less than 2mb in size');
                    return;
                }
                const file = await readFileURLAsync(relativeFile);
                editOrg({ 'logo': file, 'blob': relativeFile });
            } catch (e) {
                setImageError(e.message);
            }
        }
    }

    function handleFileChooser(event) {
        event.preventDefault();
        companyLogo.current.click();
    }

    function onFieldChange(event) {
        event.preventDefault();
        const org = { [event.target.name]: event.target.value };
        editOrg(org);
    }

    useEffect(() => {
        const checkNameValidity = () => {
            if (handle) {
                // this needs to check all handle availabilities, not just account usernames.
                axios.get(`/account/checkUsername?username=${handle}`).then(({ available }) => {
                    setAvailable(available);
                });
            } else {
                setAvailable(true);
            }
        }

        let timeoutFunction = setTimeout(() => {
            checkNameValidity();
        }, 500);
        return () => clearTimeout(timeoutFunction);
    }, [handle]);

    function editOrg(newData) {
        edit({ 'organization': { ...organization, ...newData } });
    }

    function handleSubmit(event) {
        event.preventDefault();
        if (available) {
            next();
        }
    }

    return (
        <div>
            <p className='text-large mb-2'>Setup your organization to start sharing updates with investors!</p>
            {imageError && <Toast className='toast-error'>{imageError}</Toast>}
            <form onSubmit={handleSubmit}>
                <Columns>
                    <Column className='col-3 text-center'>
                        <label className='form-label'>Logo</label>
                        <div className='text-center'>
                            <img src={logo ? logo : 'https://ui-avatars.com/api/?name=H&background=FFE2B2&color=373952'} width={80} height={80} className='s-circle' alt='Company Logo' />
                            <Button className='btn-link' onClick={handleFileChooser}>Choose File</Button>
                        </div>
                        <Input
                            type='file'
                            ref={companyLogo}
                            onChange={handleLogoChange}
                            style={{ display: 'none' }}
                            accept='.jpeg,.jpg,.png'
                        />
                    </Column>
                    <Column className='col-9 col-sm-12'>
                        <FormGroup>
                            <Input
                                name='name'
                                label='Name'
                                type='text'
                                maxLength={50}
                                placeholder='Hocus'
                                ref={companyName}
                                value={name || ''}
                                onChange={onFieldChange}
                                required
                            />
                        </FormGroup>
                        <FormGroup className={classNames(available ? '' : 'has-error')}>
                            <Input
                                name='handle'
                                label='Handle'
                                type='text'
                                pattern='^(\w){1,15}$'
                                maxLength={15}
                                placeholder='@hocus'
                                value={handle || ''}
                                onChange={onFieldChange}
                                ref={companyHandle}
                                required
                            />
                            {!available && <p className='form-input-hint'>That handle is already taken!</p>}
                        </FormGroup>
                    </Column>
                </Columns>
                <div className='text-right'>
                    <Button type='submit' className='btn-lg btn-yellow'>Next</Button>
                </div>
            </form>
        </div>
    )
}

export default FounderOnboarding;

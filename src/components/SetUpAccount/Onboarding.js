import React, { useEffect } from 'react';
import axios from '../../api/request';

import WinCard from '../design/Card/WinCard';
import Container from '../design/Container';
import SingleContainer from '../design/Container/SingleContainer';
import Divider from '../design/Divider';
import ButtonGroup from '../design/Button/ButtonGroup';
import Button from '../design/Button';
import Columns from '../design/Columns';
import Column from '../design/Columns/Column';
import FounderOnboarding from './FounderOnboarding';

import { default as HocusLogo } from '../../assets/hocus.png';
import { default as Horris } from '../../assets/horris.jpg';
import StraightToInvite from './StraightToInvite';
import { useSetUpActions, useSetUpState } from './setup';

const types = {
    'investor': {
        name: 'investor',
        display: 'an investor',
        component: <StraightToInvite />
    },
    'founder': {
        name: 'founder',
        display: 'a founder',
        component: <FounderOnboarding />
    },
    'other': {
        name: 'other',
        display: 'other',
        component: <StraightToInvite />
    }
};

function Onboarding() {
    const { selected } = useSetUpState();
    const { edit } = useSetUpActions();

    useEffect(() => {
        axios.get('/account/inviter/info').then(({ inviter, org }) => {
            edit({ 'inviteInfo': { inviter, org } });
        });
    }, [edit]);

    function handleType(e) {
        e.preventDefault();
        const value = e.target.attributes.getNamedItem('value').value;
        edit({ selected: value });
    }

    const buttons = Object.keys(types).map(key => {
        const type = types[key];
        return (
            <Button
                key={type.name}
                value={type.name}
                className={selected === type.name ? 'btn-yellow' : ''}
                onClick={handleType}>
                {type.display}
            </Button>
        )
    });

    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <Columns>
                    <Column className='col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <WinCard orgName='Hocus' userName='Horris H.' orgLogo={HocusLogo} avatar={Horris} timeAgo='1m'>
                            <p className='text-large mb-2'><span className='text-bold'>Welcome to Hocus!</span> We need just a few more pieces of info to give you the best possible Hocus experience!</p>
                            <label className='form-label'>You are:</label>
                            <ButtonGroup className='btn-group-block'>
                                {buttons}
                            </ButtonGroup>
                            {selected && <>
                                <Divider />
                                {types[selected].component}
                            </>}
                        </WinCard>
                    </Column>
                </Columns>
            </Container>
        </SingleContainer>
    )
}

export default Onboarding;
import React from 'react'
import Card from '.';
import CardBody from './CardBody';
import CardHeader from './CardHeader';

function AskCard({ avatar, userName, orgLogo, orgName, timeAgo, children }) {
    return (
        <Card className='card-red'>
            <CardHeader>
                <div className="d-flex flex-split">
                    <div className="d-flex flex-c-center">
                        <img className="img-sm s-circle mr-2" src={avatar} alt='Avatar' />
                        <div>
                            <p className="text-bold mb-0">{userName}</p>
                            <div className="d-flex flex-c-center">
                                <img className="img-xs d-inline-block s-circle mr-2" src={orgLogo} alt='Org. Logo' />
                                <p className="d-inline-block mb-0">{orgName}</p>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex flex-c-center">
                        <p className="mb-0 mr-2 text-gray-dark">{timeAgo}</p>
                        <div className="s-circle d-flex flex-v-center flex-c-center bg-red" style={{ height: '48px', width: '48px' }}>
                            <h3 className="mb-0">❓</h3>
                        </div>
                    </div>
                </div>
            </CardHeader>
            <CardBody>
                {children}
            </CardBody>
        </Card>
    )
}

export default React.memo(AskCard);
import React, { useState } from 'react';
import axios from '../api/request';
import classNames from 'classnames';

import AskCard from './design/Card/AskCard';
import CommentsIcon from './icons/CommentsIcon';
import LikesIcon from './icons/LikesIcon';
import ListIcon from './icons/ListIcon';
import TwitterIcon from './icons/TwitterIcon';
import SingleContainer from './design/Container/SingleContainer';
import Container from './design/Container';
import LinkButton from './design/Button/LinkButton';
import Button from './design/Button';

import { default as hocusLogo } from '../assets/hocus.png';
import { default as horris } from '../assets/horris.jpg';

function WaitListPage() {
    const [waiting, setWaiting] = useState(false);

    function joinWaitList() {
        setWaiting(true);

        axios.post('/waitlist/twitter/token')
            .then(({ token }) => {
                const url = `https://api.twitter.com/oauth/authenticate?oauth_token=${token}`;
                window.location = url;
            });
    }

    const earlyAccessText = waiting ? 'Contacting Twitter...' : 'Get Early Access';

    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className="columns">
                    <div className="column col-8 col-lg-10 col-sm-12 col-mx-auto">
                        <div className="text-center">
                            <h1>✨ 🔮 ✨</h1>
                        </div>
                        <AskCard userName='Horris H.' orgName='Hocus' timeAgo='5m' orgLogo={hocusLogo} avatar={horris}>
                            <p className="text-large mb-4"><span className="text-bold">I'm looking to help</span> build better relationships between investors and founders.</p>
                            <div className="d-flex flex-split">
                                <div>
                                    <LinkButton href='https://twitter.com/intent/follow?screen_name=joinhocus' size='btn-lg' color='btn-red' className='btn-action s-circle'>
                                        <LikesIcon />
                                    </LinkButton>
                                    <p className="text-gray-dark mb-0 mr-4 d-inline-block">12</p>
                                    <LinkButton href='https://twitter.com/intent/tweet?text=I%27m%20ready%20to%20hocus%20%F0%9F%94%AE%20@joinhocus' size='btn-lg' color='btn-blue' className='btn-action s-circle'>
                                        <CommentsIcon />
                                    </LinkButton>
                                    <p className="text-gray-dark mb-0 mr-4 d-inline-block">4</p>
                                </div>
                                <Button size='btn-lg' color='btn-green' className='btn-action s-circle'>
                                    <ListIcon />
                                </Button>
                            </div>
                        </AskCard>
                        <div className="mt-8 text-center">
                            <p className="text-small m-4">Get early access to help Horris.</p>
                            <button className={classNames('btn btn-twitter', waiting ? 'disabled' : '')} onClick={joinWaitList}><TwitterIcon /> {earlyAccessText}</button>
                            {/* <p className="text-small m-4">Already Hocusing? <a href="https://twitter.com/joinhocus">Sign in</a>.</p> */}
                        </div>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default WaitListPage;
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import useQueryParams from '../hooks/queryParams';
import axios from '../api/request';

import SingleContainer from './design/Container/SingleContainer';
import Container from './design/Container';
import ThankCard from './design/Card/ThankCard';
import LinkButton from './design/Button/LinkButton';
import LikesIcon from './icons/LikesIcon';
import CommentsIcon from './icons/CommentsIcon';
import ListIcon from './icons/ListIcon';
import Button from './design/Button';

import { default as hocusLogo } from '../assets/hocus.png';
import { default as horris } from '../assets/horris.jpg';

function WaitListJoinPage() {
    const history = useHistory();
    const { oauth_token, oauth_verifier } = useQueryParams();
    const [user, setUser] = useState(null);
    const [existed, setExisted] = useState(false);

    const redirect = () => history.replace('/');

    useEffect(() => {
        if (!oauth_token) {
            return redirect();
        }

        axios.post('/waitlist/twitter/verify', {
            token: oauth_token,
            verify: oauth_verifier
        }).then(({ twitter, existed }) => {
            setUser(twitter);
            if (existed) {
                setExisted(true);
            }
        });
        // this ensures that it's only rendered once as we're providing an empty dependency list it won't listen to
        // any specific objects.
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (!user) {
        return (
            <SingleContainer className='d-flex flex-v-center flex-c-center'>
                <Container className='grid-lg'>
                    <div className='columns'>
                        <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                            <ThankCard orgName='Hocus' orgLogo={hocusLogo} avatar={horris} userName='Horris H.'>
                                <p className='text-large mb-4'><span className='text-bold'>Validating Twitter information...</span></p>
                            </ThankCard>
                        </div>
                    </div>
                </Container>
            </SingleContainer>
        )
    }

    if (existed) {
        return (
            <SingleContainer className='d-flex flex-v-center flex-c-center'>
                <Container className='grid-lg'>
                    <div className='columns'>
                        <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                            <ThankCard orgName='Hocus' orgLogo={hocusLogo} avatar={horris} userName='Horris H.' timeAgo='5m'>
                                {/* eslint-disable-next-line */}
                                <p className='text-large mb-4'><span className='text-bold'>You're already on the waitlist <a className='user-tag'>@{user}</a></span>! I'll let you know when you've got access to Hocus. <a href="https://twitter.com/joinhocus">Follow us on Twitter</a> to stay up to date and make sure you don't miss any magical moments ✨</p>
                                <div className="d-flex flex-split">
                                    <div>
                                        <LinkButton href='https://twitter.com/intent/follow?screen_name=joinhocus' size='btn-lg' color='btn-red' className='btn-action s-circle'>
                                            <LikesIcon />
                                        </LinkButton>
                                        <p className="text-gray-dark mb-0 mr-4 d-inline-block">12</p>
                                        <LinkButton href='https://twitter.com/intent/tweet?text=I%27m%20ready%20to%20hocus%20%F0%9F%94%AE%20@joinhocus' size='btn-lg' color='btn-blue' className='btn-action s-circle'>
                                            <CommentsIcon />
                                        </LinkButton>
                                        <p className="text-gray-dark mb-0 mr-4 d-inline-block">4</p>
                                    </div>
                                    <Button size='btn-lg' color='btn-green' className='btn-action s-circle'>
                                        <ListIcon />
                                    </Button>
                                </div>
                            </ThankCard>
                        </div>
                    </div>
                </Container>
            </SingleContainer>
        )
    }

    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <div className='columns'>
                    <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <ThankCard orgName='Hocus' orgLogo={hocusLogo} avatar={horris} userName='Horris H.' timeAgo='5m'>
                            {/* eslint-disable-next-line */}
                            <p className='text-large mb-4'><span className='text-bold'>Thank you for joining the waitlist <a className='user-tag'>@{user}</a></span>! I'll let you know when you've got access to Hocus. <a href="https://twitter.com/joinhocus">Follow us on Twitter</a> to stay up to date and make sure you don't miss any magical moments ✨</p>
                            <div className="d-flex flex-split">
                                <div>
                                    <LinkButton href='https://twitter.com/intent/follow?screen_name=joinhocus' size='btn-lg' color='btn-red' className='btn-action s-circle'>
                                        <LikesIcon />
                                    </LinkButton>
                                    <p className="text-gray-dark mb-0 mr-4 d-inline-block">12</p>
                                    <LinkButton href='https://twitter.com/intent/tweet?text=I%27m%20ready%20to%20hocus%20%F0%9F%94%AE%20@joinhocus' size='btn-lg' color='btn-blue' className='btn-action s-circle'>
                                        <CommentsIcon />
                                    </LinkButton>
                                    <p className="text-gray-dark mb-0 mr-4 d-inline-block">4</p>
                                </div>
                                <Button size='btn-lg' color='btn-green' className='btn-action s-circle'>
                                    <ListIcon />
                                </Button>
                            </div>
                        </ThankCard>
                    </div>
                </div>
            </Container>
        </SingleContainer>
    )
}

export default WaitListJoinPage;

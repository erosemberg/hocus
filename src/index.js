import './styles.css';

async function start() {
    const renderFn = (await import('./render')).default;

    renderFn();
}

start();
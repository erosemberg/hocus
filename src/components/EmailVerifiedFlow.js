import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from '../api/request';

import LoadingPage from './LoadingPage';
import ErrorPage from './ErrorPage';
import SingleContainer from './design/Container/SingleContainer'
import Container from './design/Container';
import ThankCard from './design/Card/ThankCard';

import { default as HocusLogo } from '../assets/hocus.png';
import { default as Horris } from '../assets/horris.jpg';

function EmailVerifiedFlow() {
    const { verification } = useParams();
    const [fetching, setFetching] = useState(true);
    const [isValid, setValid] = useState(true);
    const [wasAdded, setAdded] = useState(false);

    const history = useHistory();

    if (!verification) {
        history.push('/');
    }

    useEffect(() => {
        axios.post('/waitlist/email/validate', {
            code: verification
        }).then(({ accepted, code, joined }) => {
            if (!accepted) {
                setValid(false);
            } else {
                history.push(`/flow/sign_up?code=${code}`);
            }

            setAdded(joined);
            setFetching(false);
        });
        return () => { };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [verification, setFetching, setAdded]);

    if (wasAdded) {
        return (
            <SingleContainer className='d-flex flex-v-center flex-c-center'>
                <Container className='grid-lg'>
                    <div className='columns'>
                        <div className='column col-8 col-lg-10 col-sm-12 col-mx-auto'>
                            <ThankCard orgLogo={HocusLogo} avatar={Horris} orgName='Hocus' userName='Horris H.' timeAgo='5m'>
                                { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                <p className='text-large mb-4'><span className='text-bold'>You've been invited to Hocus!</span> It seems like you tried to join using someone else's invite, you have been added to the waitlist so you can enjoy Hocus yourself!</p>
                            </ThankCard>
                        </div>
                    </div>
                </Container>
            </SingleContainer>
        )
    }

    if (!isValid) {
        return (
            <ErrorPage>
                Seems like you haven't been accepted into Hocus yet, try again later. If you think this is a warning, try using the invite link again!
            </ErrorPage>
        )
    }

    if (fetching) {
        return (
            <LoadingPage />
        )
    }
}

export default EmailVerifiedFlow;

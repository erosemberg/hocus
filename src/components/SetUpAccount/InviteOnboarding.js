import React from 'react';

import WinCard from '../design/Card/WinCard';
import Columns from '../design/Columns';
import Column from '../design/Columns/Column';
import Container from '../design/Container';
import SingleContainer from '../design/Container/SingleContainer';
import Button from '../design/Button';
import { useMultiStepState } from '../MultiStep';
import { useSetUpState } from './setup';

import { default as HocusLogo } from '../../assets/hocus.png';
import { default as Horris } from '../../assets/horris.jpg';

function InviteOnboarding() {
    const { inviteInfo } = useSetUpState();
    const { inviter, org } = inviteInfo;
    const { prev, next } = useMultiStepState();

    return (
        <SingleContainer className='d-flex flex-v-center flex-c-center'>
            <Container className='grid-lg'>
                <Columns>
                    <Column className='col-8 col-lg-10 col-sm-12 col-mx-auto'>
                        <WinCard avatar={Horris} orgLogo={HocusLogo} orgName='Hocus' userName='Horris H.' timeAgo='1m'>
                            <p className='text-large'><span className='text-bold'>Hocus pocus it's time to focus!</span> Hocus is all about building better relationships between founders and investors, starting with more transparency and real time updates.</p>
                            { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                            <p className='text-large'>You were invited by <a className='user-tag'>@{inviter}</a> so that you can follow updates from <a className='user-tag'>@{org}</a>. To get the best value from Hocus, we recommend you invite other companies that you are invested in or advising so you can also follow their updates!</p>
                            <p className='text-large'>You've got <span className='text-bold'>3 invites</span>! Enter the Twitter handles of 3 founders you'd like to invite to Hocus.</p>
                            <div className='d-flex flex-split'>
                                <Button onClick={prev} className='btn-lg'>Go Back</Button>
                                <Button onClick={next} className='btn-lg btn-yellow'>Setup Account</Button>
                            </div>
                        </WinCard>
                    </Column>
                </Columns>
            </Container>
        </SingleContainer>
    )
}

export default InviteOnboarding;
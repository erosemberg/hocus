import React, { useState, useEffect } from 'react';
import axios from '../../api/request';
import useQueryParams from '../../hooks/queryParams';
import ErrorPage from '../ErrorPage';
import LoadingPage from '../LoadingPage';
import CreateAccountPage from './CreateAccount';

function SignUp() {
    const { code } = useQueryParams();
    const [isValid, setIsValid] = useState(false);
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        axios.post('/account/code/validate', {
            code
        }).then(({ valid }) => {
            setIsValid(valid);
            setLoading(false);
        })

        return () => { };
    }, [code]);

    if (isLoading) {
        return (
            <LoadingPage />
        )
    }

    if (!isValid) {
        return (
            <ErrorPage>
                It seems like the invite you provided was already claimed or invalid!
            </ErrorPage>
        )
    }

    // the multi step provider automatically takes care of all the step handling for us
    // so basically each step will be able to access the state of all the other ones because
    // we're already wrapped in a SignUpFlowProvider
    return (
        <CreateAccountPage inviteCode={code} />
    )
}

export default SignUp;

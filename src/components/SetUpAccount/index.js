import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useUser } from '../../api/user';
import { SetUpFlowProvider } from './setup';
import { MultiStepProvider, Step } from '../MultiStep';
import Onboarding from './Onboarding';
import InviteOnboarding from './InviteOnboarding';
import SubmitAccountForSetup from './SubmitAccountForSetup';
import LoadingPage from '../LoadingPage';

function SetUpAccount() {
    const { finishedSetup } = useUser();
    const history = useHistory();

    useEffect(() => {
        // don't let accounts which have already finished their setup do weird stuff here, so we push them back to the main feed
        if (finishedSetup) {
            history.push('/');
        }

        return () => { };
    }, [finishedSetup, history]);

    if (finishedSetup) {
        return (
            <LoadingPage />
        )
    }

    return (
        <SetUpFlowProvider>
            <MultiStepProvider>
                <Step component={<Onboarding />} />
                <Step component={<InviteOnboarding />} />
                <Step component={<SubmitAccountForSetup />} />
            </MultiStepProvider>
        </SetUpFlowProvider>
    )
}

export default SetUpAccount;

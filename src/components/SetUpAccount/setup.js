import React, { useContext, useReducer, useMemo } from 'react';

const SetUpStateContext = React.createContext();
const SetUpActionsContext = React.createContext();

function setupReducer(state, action) {
    switch (action.type) {
        // this may change in the future so maybe it's easier to just use a free state
        case 'edit': {
            const { data } = action;

            return {
                ...state,
                ...data
            };
        }
        default: {
            throw new Error(`Unknown action type ${action.type}`);
        }
    }
}

function SetUpFlowProvider({ children }) {
    const [signup, dispatch] = useReducer(setupReducer, {});

    const actions = useMemo(() => {
        const edit = data => dispatch({ type: 'edit', data });

        return {
            edit
        }
        // dispatch shouldn't ever change so it should be fine to just use that as the dependency
        // so that eslint doesn't complain
    }, [dispatch]);

    return (
        <SetUpStateContext.Provider value={signup}>
            <SetUpActionsContext.Provider value={actions}>
                {children}
            </SetUpActionsContext.Provider>
        </SetUpStateContext.Provider>
    )
}

function useSetUpState() {
    const context = useContext(SetUpStateContext);
    if (context === undefined) {
        throw new Error('useSetUpState must be used within a SetUpFlowProvider');
    }

    return context;
}

function useSetUpActions() {
    const context = useContext(SetUpActionsContext);
    if (context === undefined) {
        throw new Error('useSetUpActions must be used within a SetUpFlowProvider');
    }

    return context;
}

export { SetUpFlowProvider, useSetUpState, useSetUpActions };